# Autonomous shutter

A motherboard with a stm32 automatically controls the closing of the shutter, depending on the brightness and temperature.  
 - Case study 1: The sun goes down, the temperature drops, the shutters close automatically to keep the privacy and isolate the windows from the cold of the night.
 - Case study 2 : The outside temperature is very high because of the sun orientation, the shutters close to keep the shade in the house and keep the coolness.

## Introduction 


## Operation 
```bash
variables
    light, temperature : nombres
    LED_R, LED_V : bool
     state_shatter : "deploy", "fold"
begin 
    LED_R take the value "ON" // initialisation; the system is powered 
    LED_V take the value "OFF"
    state_shatter take the value "fold"
    Wvile (1) : 
        read temperature 
        read light 
        IF temperature > 30 
            LED_V take the value "ON"
            State_shatter take the value "deploy"
        ELSE 
            State_shatter take the value "fold"
        END IF
        
         IF temperature < 100 
            LED_V take the value "ON"
            State_shatter take the value "deploy"
        ELSE 
            State_shatter take the value "fold"
        END IF       

fin algorithme
```

## Hardare used 
 - STM32 
 - 

## Software used 
- [GitLab](https://gitlab.com/KlemClem/autonomous-shutter)
- [Eagle](https://www.autodesk.fr/products/eagle/overview)

